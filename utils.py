import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import xgboost as xgb
from sklearn.metrics import mean_squared_error
from sklearn.metrics import f1_score
import json
from sklearn.metrics import confusion_matrix
from functools import partial
import optuna
from sklearn.preprocessing import StandardScaler
import numpy as np
import torch

def step(value,thr=0.5):
    if value >= thr:
        return 1
    else:
        return 0
    
# Fución para obtimizar los hiperparametros del modelo
def pre_objective(trial, dtrain, dtest):
    y_train = dtrain.get_label()
    y_test = dtest.get_label()

    params = {
        "verbosity": 1,
        "nthread": 1,
        "random_state": 0,
        "objective": "binary:logistic",
        "booster": "gbtree",
        "tree_method": "hist",
        "eval_metric": "auc",
        "max_depth": trial.suggest_int("max_depth", 2, 15),
        "learning_rate": trial.suggest_loguniform("learning_rate", 1e-4, 10),
        "gamma": trial.suggest_loguniform("gamma", 1e-1, 100),
        "min_child_weight": trial.suggest_int("min_child_weight", 0, 100),
        "max_delta_step": trial.suggest_int("max_delta_step", 0, 100),
        "subsample": trial.suggest_uniform("subsample", 0.5, 1),
        "colsample_bytree": trial.suggest_uniform("colsample_bytree", 0.5, 1),
        "colsample_bylevel": trial.suggest_uniform("colsample_bylevel", 0.5, 1),
        "colsample_bynode": trial.suggest_uniform("colsample_bynode", 0.5, 1),
        "reg_alpha": trial.suggest_loguniform("reg_alpha", 1e-8, 1e2),
        "reg_lambda": trial.suggest_loguniform("reg_lambda", 1e-8, 1e2),
    }

    num_round = trial.suggest_int("num_round", 10, 500)

    try:
        bst = xgb.train(
            params,
            dtrain,
            num_round,
            evals=[(dtest, "eval"), (dtrain, "train")],
            early_stopping_rounds=10,
            verbose_eval=False,
        )
        preds_test = bst.predict(dtest, ntree_limit=bst.best_ntree_limit)
        preds_train = bst.predict(dtrain, ntree_limit=bst.best_ntree_limit)
        y_pred_test = [step(a) for a in  preds_test]
        y_pred_train = [step(a) for a in  preds_train]
        to_max_1 = f1_score(y_pred_test, y_test)
        to_max_2 = f1_score(y_pred_train, y_train)
        print("Error in test: ", to_max_1, ", Error in train: ", to_max_2)
        err = - to_max_2 +abs(to_max_1 -to_max_2) #Para evitar overfitting del modelo
        print(err)
        print("---------------")
    except Exception as e:
        print(e)
        err = 0
    return err

def tune_classifier(dtrain, dtest, name, n_trials):
    objective = partial(pre_objective, dtrain=dtrain, dtest=dtest)
    study = optuna.create_study(
        study_name="rappi_{0}".format(name),
        storage="sqlite:///models/model_{0}.db".format(name),
        load_if_exists=True,
        direction = "minimize"
    )
    try:
        df = study.trials_dataframe(attrs=('number', 'value', 'params', 'state'))
        current_trials = df.number.max() + 1
        n_trials = n_trials - current_trials
    except: 
        pass
    print("Tuning regressor!.....")
    print(name, n_trials)
    if n_trials > 0:
        study.optimize(objective, n_trials=n_trials)
    return study.best_params

def binary_acc(y_pred, y_test):
    y_pred_tag = torch.round(torch.sigmoid(y_pred))

    correct_results_sum = (y_pred_tag == y_test).sum().float()
    acc = correct_results_sum/y_test.shape[0]
    acc = torch.round(acc * 100)
    
    return acc