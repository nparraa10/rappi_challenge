# Rappi_challenge

## Requerimientos

Para instalar todas las librerías necesarias por favor dentro de la carpeta principal ejecute el siguiente comando:

```
pipenv install
```
teniendo en cuenta que se debe tener instalado `python==3.9`, en otro caso dirijase al archivo `pipfile`y cambie la version a la que sea de su preferencia, aunque podrían haber errores en las versiones de las liberías. Para activar el ambiente ejecute:

```
pipenv shell
```
## Análisis

Todos los análisis correspondientes a la data se encuentran en el notebook `Analisis.ipynb`, con las descripciones correspondientes y los insights. 

## Modelo

Toda el código correspondiente al modelo se ecuentra en `models.py` y `utils.py`. La explicación, los análisis del modelo y sus resultados se encuetran dentro de `models_train.ipynb`.

## Producción del modelo
Aunque en `models.py` es clara la implementación, dentro de `predict.py` se puede hacer un rapido uso del modelo solo ejecutando en la terminal:
```
python predict.py
```
En casos de querer usar otros datos, solo es cambiar el dataframe que se carga.


