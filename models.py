import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.metrics import f1_score
import json
from sklearn.metrics import confusion_matrix
from functools import partial
import optuna
from sklearn.preprocessing import StandardScaler
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import xgboost as xgb
from utils import *

### Perceptron con pytorch
class Perceptron(nn.Module):
    def __init__(self):
        super(Perceptron, self).__init__()
        # Number of input features is 12.
        self.layer_1 = nn.Linear(34, 64) 
        self.layer_2 = nn.Linear(64, 64)
        self.layer_out = nn.Linear(64, 1) 
        
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(p=0.1)
        self.batchnorm1 = nn.BatchNorm1d(64)
        self.batchnorm2 = nn.BatchNorm1d(64)
        
    def forward(self, inputs):
        x = self.relu(self.layer_1(inputs))
        x = self.batchnorm1(x)
        x = self.relu(self.layer_2(x))
        x = self.batchnorm2(x)
        x = self.dropout(x)
        x = self.layer_out(x)
        
        return x
class trainData(Dataset):
    
    def __init__(self, X_data, y_data):
        self.X_data = X_data
        self.y_data = y_data
        
    def __getitem__(self, index):
        return self.X_data[index], self.y_data[index]
        
    def __len__ (self):
        return len(self.X_data)


## test data    
class testData(Dataset):
    
    def __init__(self, X_data):
        self.X_data = X_data
        
    def __getitem__(self, index):
        return self.X_data[index]
        
    def __len__ (self):
        return len(self.X_data)


### Clase para entrenar y predecir fraudes
class Rappi_fraude:
    """
    Function to train a model and predict fraud in consumers

    """
    
    def __init__(self, data = None):
        self._data = data
        self._Xtrain = None
        self._Ytrain = None
        self._best_params = None
        self._nn_model = None
        self._xgb_model = None
        
    def parsing(self,label = "fraude",balance=True,columns=["genero","hora","monto","establecimiento","ciudad","tipo_tc","linea_tc","interes_tc","status_txn",'is_prime', 'dcto', 'cashback','dispositivo'], dummy_na=True):
        """
        Function to Parsing data
        """
        if "dispositivo" in columns:
            self._data["dispositivo"] = self._data.dispositivo.apply(lambda x: eval(x)['os'])
        if balance:
            df_1 = self._data[self._data[label] == True]
            df_0 = self._data[self._data[label] == False]
            self._data = pd.concat([df_1,df_0.sample(n=df_1.shape[0])])
            
        self._Xtrain = pd.get_dummies(self._data[columns], dummy_na=dummy_na)
        self._Ytrain = self._data[label].replace(True,1).replace(False,0)
        
    
    def train_xgb(self,test_size=0.30, random_state=42,num_round=100,tune=None,n_trials=20):
        """
        Function to train a xgboost model
        """
        if tune:
            X_train, X_test, y_train, y_test = train_test_split(self._Xtrain, self._Ytrain, test_size=0.33, random_state=42)
            #Build dmatrix xgboost
            dtest = xgb.DMatrix(X_test, label=y_test)
            dtrain = xgb.DMatrix(X_train, label=y_train)
            self._best_params = tune_classifier(dtrain, dtest,tune, n_trials)
            self._xgb_model = xgb.train(self._best_params,
                dtrain,
                self._best_params['num_round'],
                evals=[(dtest, "eval"), (dtrain, "train")],
                early_stopping_rounds=10,
                verbose_eval=False,
            )
            self._Xtrain2 = X_train
        
        else:
            X_train, X_test, y_train, y_test = train_test_split(self._Xtrain, self._Ytrain, test_size=test_size, random_state=random_state)

            self._Xtrain2 = X_train

            #Build dmatrix xgboost
            dtest = xgb.DMatrix(X_test, label=y_test)
            dtrain = xgb.DMatrix(X_train, label=y_train)
            params = {"booster":"gbtree", "max_depth": 6, "eta": 0.4, "objective": "binary:logistic", "nthread":3,"eval_metric": "auc"}
            self._xgb_model = xgb.train(params,
                dtrain,
                num_round,
                evals=[(dtest, "eval"), (dtrain, "train")],
                early_stopping_rounds=10,
                verbose_eval=False,
            )
            
        self._preds_test = self._xgb_model.predict(dtest, ntree_limit=self._xgb_model.best_ntree_limit)
        self._preds_train = self._xgb_model.predict(dtrain, ntree_limit=self._xgb_model.best_ntree_limit)
            
            
        y_pred_test = [step(a) for a in  self._preds_test]
        y_pred_train = [step(a) for a in  self._preds_train]
        
        
        return y_pred_test, y_test, y_pred_train, y_train
        
    
    def train_nn(self,EPOCHS = 50,BATCH_SIZE = 64,LEARNING_RATE = 0.001):
        """
        Function to train neural network
        """
        scaler = StandardScaler()
        scaler.fit(self._Xtrain)
        x_train = scaler.transform(self._Xtrain)
        X_train, X_test, y_train, y_test = train_test_split(x_train, self._Ytrain.values, test_size=0.33, random_state=42)
        self._Xtrain3 = X_train
        train_data = trainData(torch.FloatTensor(X_train), torch.FloatTensor(y_train))
        test_data = testData(torch.FloatTensor(X_test))
        train_loader = DataLoader(dataset=train_data, batch_size=BATCH_SIZE, shuffle=True)
        test_loader = DataLoader(dataset=test_data, batch_size=1)
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self._nn_model = Perceptron()
        self._nn_model.to(device)
        criterion = nn.BCEWithLogitsLoss()
        optimizer = optim.Adam(self._nn_model.parameters(), lr=LEARNING_RATE)
        self._nn_model.train()
        for e in range(1, EPOCHS+1):
            epoch_loss = 0
            epoch_acc = 0
            for X_batch, y_batch in train_loader:
                X_batch, y_batch = X_batch.to(device), y_batch.to(device)
                optimizer.zero_grad()

                y_pred = self._nn_model(X_batch)

                loss = criterion(y_pred, y_batch.unsqueeze(1))
                acc = binary_acc(y_pred, y_batch.unsqueeze(1))

                loss.backward()
                optimizer.step()

                epoch_loss += loss.item()
                epoch_acc += acc.item()

            print(f'Epoch {e+0:03}: | Loss: {epoch_loss/len(train_loader):.5f} | Acc: {epoch_acc/len(train_loader):.3f}')
            y_pred_list = []
            self._nn_model.eval()
            with torch.no_grad():
                for X_batch in test_loader:
                    X_batch = X_batch.to(device)
                    y_test_pred = self._nn_model(X_batch)
                    y_test_pred = torch.sigmoid(y_test_pred)
                    y_pred_tag = torch.round(y_test_pred)
                    y_pred_list.append(y_pred_tag.cpu().numpy())

            y_pred_list = [a.squeeze().tolist() for a in y_pred_list]
            
        return y_pred_list, y_test
        
    def predict_xgb(self,data):
        """
        Function to predict using xgboost model
        """
        ddata = xgb.DMatrix(data)
        preds = self._xgb_model.predict(ddata, ntree_limit=self._xgb_model.best_ntree_limit)
        try:
            return [step(a) for a in  preds]
        except:
            return step(preds)

    def save_xgb(self,path="model.model"):
        self._xgb_model.save_model(path)
    
    def load_xgb(self,path):
        self._xgb_model = xgb.Booster()
        self._xgb_model.load_model(path)


