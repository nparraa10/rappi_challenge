import pandas as pd
from utils import *
from models import *

# Declaración de la clase construida
df = pd.read_csv("ds_challenge_2021.csv")
rappi = Rappi_fraude(df)

# Balanceo y alistamiento de los datos.
columns = ["genero","hora","monto","establecimiento","ciudad","tipo_tc","linea_tc","interes_tc","status_txn",'is_prime', 'dcto', 'cashback','dispositivo']
rappi.parsing(label = "fraude",balance=True,columns=columns, dummy_na=True)

# load
rappi.load_xgb("models/final.model")

predicciones = rappi.predict_xgb(pd.get_dummies(df[columns], dummy_na=True).iloc[0:10])

print(predicciones)